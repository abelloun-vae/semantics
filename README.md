# ENGLISH

# Lambda calculus with Peano numbers
Written in the Eloq language, the goal is to experiment expression of multiple semantics using a programming style that allows one to separate the semantics from its interpretation.
For example, beside the classical interpretation, it is possible to use a "print" implementation that will display the semantics as usual logical inference rules.
It is also obviously possible to derive both big-step and small step semantics from a single semantics definition, only through variation of the interpretation (or implementationà part.


## /App : semantics definitions 
- `/App/TypeChecking` : contains the semantics for bidirectionnal Hindley/Milner typechecking algorithm,
- `/App/Type` : folder containing definition, serialization, substitution and unification of types,
- `/App/Expression` : forlder containing definition, serialization, substitution, simplification of term expressions
- `/App/Expression/Evaluation` : folder containing operationnal and denotationnal semantics, both strict and lazy.


## /libs : Lib of base data types
Combinators, Identity, Constant, Maybe, List


## /libs/Algorithms : Interpretation (or implementation) algorithms for each kind of semantics
- `/libs/Algorithms/Bases` : elementary algorithms
- `/libs/Algorithms/*` : algorithm resulting from the combination of elemetary ones in order to get the desired functionnalities.


## /tests
Toy implementation of a dependent type checking algorithm by Thierry Coquand in the early days.


## /ZFuture
Non structured, non fonctionnal. Just to give myself an idea of the direction in which I wish to evolve my languages. It covers aspects like syntax, module system, proof assitant.








# FRENCH

# Lambda Calcul avec les nombres de Peano
L'implémentation est faite dans le langage Eloq.
Le but de ce code est d'expérimenter l'expression de différentes sémantiques en utilisant un styles découplant la sémantique en elle-même de son interpretation.
Par exemple, en dehors d'une interprétation "classique" ( qui donne le resultat attendu ) il est possible d'utiliser une interprétation de type "print" qui affiche la sémantique sous forme de règles d'inférence.
Il semble aussi possible de dériver à la fois les sémantiques big-step et small-step à partir d'une seule définition, simplement en faisant varier l'interprétation.


## /App : définition des sémantiques
- `/App/TypeChecking` : fichier contenant l'algorithme de type checking Hindley/Milner en mode bidirectionnel
- `/App/Type` : dossier contenant la definition, serialisation, substitution et unification des types
- `/App/Expression` : dossier contenant la definition, serialisation, substitution, et la simplification des expression
- `/App/Expression/Evaluation` : dossier contenant des sémantiques opérationnelles et dénotationnelles, strictes et "lazy"


## /libs : type de données "de base"
Combinateurs, Identity, Constant, Maybe, List


## /libs/Algorithms : algorithmes d'interprétation pour les différents types de sémantique
- `/libs/Algorithms/Bases` : dossier contenant les algorithmes élémentaires
- `/libs/Algorithms/*` : assemblages des algorithmes élementaires pour fournir les "vrais" algorithmes


## /tests
Implémentation d'un algorithme de type-checking pour les types dépendants de Thierry Coquand


## /ZFuture
Non structuré, non fonctionnel. Pour me donner une idée de ce vers quoi doit evoluer le langage : sa syntaxe, le système de module, la possibilité de "type-checker" des preuves à la manière d'agda ou idris.
